package main;

public class BubbleSort {

	public BubbleSort() {
		// TODO Auto-generated constructor stub
	}
	
	public static void main(String[] args) {
		int[] arr = new int[]{2,4,0,5,1,3};
		
		doBubbleSort(arr);
		for(int i: arr) {
			System.out.print(i + ",");
		}
	}

	public static void doBubbleSort(int[] arr) {
		boolean swapped;
		for (int h = 0; h < arr.length - 1; h++) {
			System.out.println("+ external iteration: " + h);
			swapped = false;
			for (int i = 0; i < arr.length - 1 - h; i++) {	// length -1 because we don't need to do this for the last element (i+1 would be out of bounds)
															// length -h because each time we complete a pass, the last element
															// was already sorted on the previous pass 
				System.out.println("++ internal iteration: " + i);
				if (arr[i] > arr[i+1]) {
					int right = arr[i+1];
					arr[i+1] = arr[i];
					arr[i] = right;
					swapped = true;
					System.out.println("+++ swap occured!");
				}
			}
		if (swapped == false) {
			System.out.println("no swap occured on the internal loop - array is sorted! ");
			break;	
			}
		}
	}
	
}
