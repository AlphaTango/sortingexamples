package main;

public class QuickSort {

	public QuickSort() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] quickTest = new int[] {1,4,2,7,5,3,6};
		partitionArray(quickTest, 0, quickTest.length);
		
		for (int i : quickTest) {
			System.out.println(i);
		}

	}

	public static void partitionArray(int[] input, int start, int end) {
		
		
		if (input.length == 2) {		// if there are only two elements
			if (input[0] > input[1]) {	// swap them if need be
				int right = input[1];
				input[1] = input[0];
				input[0] = right;
			}
		} else {						// otherwise sort the array around the pivot
			int partition = input.length - 1;
			int split = 0;
			for (int i = start; i < end; i++) {
				System.out.println("inside for loop");
				if (input[i] > input[partition]) {
					int current = input[i];
					input[i] = input[partition];
					input[partition] = current;
					split++;
				}
			}
			//partitionArray(input, 0, split);
		}
	}
	
}
