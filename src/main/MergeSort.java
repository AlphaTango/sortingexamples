package main;

public class MergeSort {

	public MergeSort() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[] mergeTest = new int[] {2,4,6,1,3,7,5};
		
		int[] output = new int[7];
		int[] left = new int[] {2,4,6};
		int[] right = new int[] {1,3,7,5};

		splitArray(mergeTest);
		for (int i = 0; i < mergeTest.length; i++) {
			System.out.println(mergeTest[i]);
		}

	}
	
	public static int[] mergeArrays(int[] output, int[] left, int[] right) {
		
		int leftIndex = 0;
		int rightIndex = 0;
		
		for (int i = 0; i < output.length; i++) {
			
			if (leftIndex < left.length && rightIndex < right.length) {
				if (left[leftIndex] < right[rightIndex]) {
					System.out.println("left was smaller: " + left[leftIndex]);
					output[i] = left[leftIndex++];
					continue;
				} else {
					System.out.println("right was smaller: " + right[rightIndex]);
					output[i] = right[rightIndex++];
					continue;
				}
			} else if (leftIndex < left.length) {
				System.out.println("no elements left in right, adding from left: " + left[leftIndex]);
				output[i] = left[leftIndex++];
			} else if (rightIndex < right.length) {
				System.out.println("no elements left in left, adding from right: " + right[rightIndex]);
				output[i] = right[rightIndex++];
			}
		}
		return output;
	}
	
	public static int[] splitArray(int[] input) {

		int middle = input.length / 2;
		int[] left = new int[middle];
		int[] right = new int[input.length - middle];
		System.out.println(input.length - middle);
		
		if(input.length > 1) {
			System.out.println("Splitting array");
			
			for (int i = 0; i < left.length; i++) {
				left[i] = input[i]; 
				System.out.println("added to left: " + input[i]);
			}
			for (int j = 0; j < right.length; j++) {
				System.out.println("added to right: " + input[j + middle]);
				right[j] = input[j + middle]; 
			}
			left = splitArray(left);
			right = splitArray(right);
			mergeArrays(input, left, right);
		}
		return input;
	}

}
