/**
 * 
 */
package test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import main.BubbleSort;

/**
 * @author Leigh Plumley
 *
 */
public class BubbleSortTest {
	
	// SETUP + TEARDOWN

	/**
	 * Runs before tests begin
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("Starting unit tests for doBubbleSort() method in BubbleSort.java");
	}

	/**
	 * Runs before each individual test 
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		System.out.println("=== STARTING NEW TEST ===");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}
	
	/**
	 * Runs after all tests have completed
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("Finished unit tests for doBubbleSort() method in BubbleSort.java");
	}
	
	// TESTS

	/**
	 * Test method for {@link main.BubbleSort#doBubbleSort(int[])}.
	 */
	@Test
	public final void testDoBubbleSortSortedInput() {
		int[] testArr = new int[] {0,1,2,3,4,5,6};
		BubbleSort.doBubbleSort(testArr);
		int[] expectedArr = new int[] {0,1,2,3,4,5,6};
		
		assertArrayEquals(testArr, expectedArr);
	}
	
	/**
	 * Test method for {@link main.BubbleSort#doBubbleSort(int[])}.
	 */
	@Test
	public final void testDoBubbleSortUnSortedInput() {
		int[] testArr = new int[] {2,4,0,5,1,3};
		BubbleSort.doBubbleSort(testArr);
		int[] expectedArr = new int[] {0,1,2,3,4,5};
		
		assertArrayEquals(testArr, expectedArr);
	}

}
