/**
 * 
 */
package test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import main.MergeSort;

/**
 * @author Tulsy
 *
 */
public class MergeSortTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("Starting tests on MergeSort.java");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		System.out.println("Starting Test");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}
	
	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * Test method for {@link main.MergeSort#mergeArrays(int[], int[], int[])}.
	 */
	@Test
	public final void testMergeArrays() {
		
		int[] output = new int[7];
		int[] left = new int[] {2,4,6};
		int[] right = new int[] {1,3,7,5};
		output = MergeSort.mergeArrays(output, left, right);
		int[] expected = new int[] {1,2,3,4,6,7,5};
		
		assertArrayEquals(expected, output);
	}

	/**
	 * Test method for {@link main.MergeSort#splitArray(int[])}.
	 */
	@Test
	public final void testSplitArray() {
		
		int[] mergeTest = new int[] {2,4,6,1,3,7,5};
		int[] expected = new int[] {1,2,3,4,5,6,7};
		assertArrayEquals(expected, MergeSort.splitArray(mergeTest));
	}

}
